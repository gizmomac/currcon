class CurrencyException(Exception):
    pass


class InvalidCurrency(CurrencyException):
    pass


class CommunicationError(CurrencyException):
    pass


class MissingRateError(CurrencyException):
    pass


class InvalidAmountError(CurrencyException):
    pass
