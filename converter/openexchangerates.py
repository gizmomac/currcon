from decimal import Decimal
import requests
from converter.exceptions import CommunicationError


class OpenExchangeRates:
    URL = 'https://openexchangerates.org/api/latest.json'
    FREE_CURRENCY = 'USD'

    def __init__(self, app_id: str, currencies: str, paid: bool = False):
        self.app_id = app_id
        self.currencies = currencies
        self.paid = paid

    def get_rates(self, currency: str) -> dict:
        if self.paid:
            response = self._latest(currency)
            return {c: response.get(str(c).upper()) for c in self.currencies}
        else:
            response = self._latest(self.FREE_CURRENCY)
            return {c: Decimal(response.get(str(currency).upper())) / Decimal(response.get(str(c).upper())) for c in
                    self.currencies}

    def _latest(self, base: str) -> dict:
        try:
            resp = requests.get(self.URL, params={"app_id": self.app_id, "base": base})
            resp.raise_for_status()
            rates = resp.json(parse_int=Decimal, parse_float=Decimal)['rates']
        except requests.exceptions.RequestException as e:
            raise CommunicationError(e)
        except KeyError:
            raise CommunicationError("Rates are missing from the response")
        return rates
