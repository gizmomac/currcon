from app import redis_client
from decimal import Decimal
from converter.exceptions import MissingRateError

CURRENCIES = "currencies"


def get_rate(convert_from: str, convert_to: str) -> Decimal:
    """
    Get rate from the database
    
    :param convert_from: currency code of conversion from
    :param convert_to: currency code of conversion to
    :return: decimal conversion rate
    """
    rate = redis_client.hget(convert_from, convert_to)
    if rate is None:
        raise MissingRateError("Missing conversion rate")
    return Decimal(rate)


def set_rates(convert_from: str, rates: dict) -> dict:
    """
    Set rates to the database

    :param convert_from: currency code of conversion from
    :param rates: dict of currency code and rate pairs
    :return: dict of rates
    """
    for key, val in rates.items():
        rates[key] = str(val)
    pipe = redis_client.pipeline()
    pipe.delete(convert_from)
    pipe.hmset(convert_from, rates)
    pipe.execute()
    return redis_client.hgetall(convert_from)


def get_currencies() -> list:
    """
    Get supported currencies from the database

    :return: list of currency codes
    """
    return list(redis_client.smembers(CURRENCIES))


def add_currency(currency: str) -> list:
    """
    Add currency to available currencies
    :return: list of currency codes
    """
    redis_client.sadd(CURRENCIES, currency.lower())
    return list(redis_client.smembers(CURRENCIES))


def remove_currency(currency: str) -> list:
    """
    Remove currency to available currencies
    :return: list of currency codes
    """
    redis_client.srem(CURRENCIES, currency.lower())
    return list(redis_client.smembers(CURRENCIES))
