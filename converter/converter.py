from decimal import Decimal, InvalidOperation
from typing import Any
from converter.storage import get_rate, get_currencies
from converter.exceptions import InvalidCurrency, InvalidAmountError


def convert(amount: Any, currency_from: str, currency_to: str) -> (Decimal, Decimal):
    """
    Convert the given amount from currency_from to currency_to

    :param amount: Decimal amount to convert
    :param currency_from: currency of the amount
    :param currency_to: target currency

    :return: conversion_rate, converted amount
    """
    amount = _validate_amount(amount)
    currency_from = _validate_currency(currency_from)
    currency_to = _validate_currency(currency_to)
    conversion_rate = get_rate(currency_from, currency_to)
    return conversion_rate, conversion_rate * amount


def _validate_currency(currency: str) -> str:
    """
    Validate if currency is supported.

    :param currency: string currency code
    :return: boolean True if supported, False if not
    """
    currencies = get_currencies()
    if not currency.lower() in currencies:
        raise InvalidCurrency(f"Invalid currency_code: {currency}")

    return currency.lower()


def _validate_amount(amount: Any) -> Decimal:
    """
    Validate amount

    :param amount: converting amount
    :return: Decimal amount
    """
    try:
        return Decimal(amount)
    except InvalidOperation:
        raise InvalidAmountError(f"Invalid amount: {amount}")
