from flask import Blueprint, jsonify, request, Response
from datetime import datetime
from converter.exceptions import CurrencyException

api = Blueprint('api', __name__)


@api.route("/api/convert/<amount>/<currency_from>/<currency_to>", methods=["GET"])
def convert(amount: str, currency_from: str, currency_to: str) -> (Response, int):
    """
    Converter api, convert the given amount from the given currency to the target currency.

    :param amount: Amount to convert
    :param currency_from: Convert from currency
    :param currency_to: Convert to currency
    :return: json response
    """
    from converter.converter import convert

    response = {
        "request": {
            "query": request.base_url,
            "amount": amount,
            "from": currency_from.upper(),
            "to": currency_to.upper()
        },
        "meta": {
            "timestamp": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
        }
    }
    try:
        rate, amount = convert(amount, currency_from, currency_to)
    except CurrencyException as e:
        response["error"] = str(e)
        status_code = 400
    else:
        response["meta"]["rate"] = rate
        response["response"] = amount
        status_code = 200

    return jsonify(response), status_code
