import os
from flask import Flask
from flask_redis import FlaskRedis
from converter.api import api

app = Flask(__name__)
app.config.from_object(os.environ.get('CONFIG', 'settings.dev'))
app.register_blueprint(api)

redis_client = FlaskRedis(app, decode_responses=True)

if __name__ == "__main__":
    app.run()
