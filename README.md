# Currency Converter API


## Architecture:

My idea was to keep it simple. To finish the task I'll need 2 things,
an API with one single conversion endpoint, and a CLI command to manage
the available currencies, and update the conversions rates.


### Technologies:

I decided to go with Flask + Redis. The api is very lightweight it does not require any heavier
framework. For database Redis provide persistence and it's extremely fast. 


### Files:

The project is very small, so i decided to leave the file structure as flat as possible.

* app.py - The flask application.
* cli.py - The command line interface.
* converter/api.py - The conversion endpoint.
* converter/converter.py - The business logic.
* converter/exceptions.py - The internal exceptions.
* converter/openexchangerates.py - Client to communicate with external API.
* converter/storage.py - Communicatie with the Database.


### API:

Api is very similar for the openexchangerate one.


### Challenges and way to improve it:

* I wanted to keep the option to set different exchange rates depend on the conversion direction, that the reason
why store the rates per currency.

* The mentioned API (openexchangerate) in free version doesn't support custom Base currency rate.
Fist I wanted to simplify so I bought the paid API, later I extended the class to be able to use the free API.

* Sentry, or other error tracking

* Better logging

* Store what time when the rate was last time updated

* Dockerize, or add setup for the running environment.

* More robust rate fetching

* Make rounding configurable from the api

* Mock redis (so the tests can pass without redis)

* Change to pytest 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.


### Prerequisites

Redis, Python 3.7 and Virtualenv


### Installing

Clone the project, create the env and install the requirements

```
git clone https://bitbucket.org/gizmomac/currcon.git
cd currcon
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```


### Setup with CLI

Add USD, EUR, PLN and CZK and fetch the current conversion rates.

```
python3 cli.py add_currency USD
python3 cli.py add_currency EUR
python3 cli.py add_currency PLN
python3 cli.py add_currency CZK

python3 cli.py update_rates
```


### Start Flask API

To run the server locally
```
python3 run.py 
```

To test it

```
curl http://127.0.0.1:8080/api/convert/333/eur/usd
```

For coverage

```
coverage run
coverage html
```