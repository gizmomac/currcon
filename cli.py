from converter import storage
import baker
from converter.openexchangerates import OpenExchangeRates
from app import app


@baker.command
def get_currencies():
    """Get currencies from database.

    Example: python3 cli.py get_currencies
    > ['eur', 'usd', 'huf']
    """
    return storage.get_currencies()


@baker.command(params={"code": "Currency code, example: eur"})
def add_currency(code):
    """
    Add currency to database, return the existing currencies.

    Example: python3 cli.py add_currency eur
    > ['eur', 'usd', 'huf']
    """
    return storage.add_currency(code)


@baker.command(params={"code": "Currency code, example: eur"})
def remove_currency(code):
    """
    Remove currency to database, return the existing currencies.

    Example: python3 cli.py remove_currency eur
    > ['usd', 'huf']
    """
    return storage.remove_currency(code)


@baker.command
def update_rates():
    """
    Fetch latest rates from openexchangerates.org,
    return the fetched and updated rates.

    Example: python3 cli.py add_currency eur
    > {'usd': {'usd': '1', 'huf': '0.003385469564628613988760241045', 'eur': '1.123630855802205013191426247'},
    'huf': {'usd': '295.38', 'huf': '1', 'eur': '331.8980821868553167964834849'}, 'eur': {'usd': '0.889972',
    'huf': '0.003012973119371656848804929244', 'eur': '1'}}
    """
    all_rates = {}
    currencies = storage.get_currencies()
    client = OpenExchangeRates(app.config['OPENEXCHANGERATE_APP_ID'], currencies)
    for currency in currencies:
        rates = client.get_rates(currency)
        storage.set_rates(currency, rates)
        all_rates[currency] = rates
    return all_rates


baker.run()
