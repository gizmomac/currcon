import mock
from decimal import Decimal
from tests.base import TestBase
from cli import get_currencies, add_currency, update_rates, remove_currency
from converter.openexchangerates import OpenExchangeRates


class CliTest(TestBase):

    @mock.patch("converter.storage.get_currencies")
    def test_get_currencies_no_currency(self, mock_get_currencies):
        return_value = [self.EUR, self.USD, self.HUF]
        mock_get_currencies.return_value = return_value
        currencies = get_currencies()
        self.assertEqual(mock_get_currencies.call_count, 1)
        self.assertEqual(currencies, return_value)

    @mock.patch("converter.storage.add_currency")
    def test_add_currency(self, mock_add_currency):
        mock_add_currency.return_value = [self.EUR]
        resp = add_currency(self.EUR)
        self.assertEqual(mock_add_currency.call_count, 1)
        self.assertEqual(mock_add_currency.call_args[0][0], self.EUR)
        self.assertEqual(resp, mock_add_currency.return_value)

    @mock.patch("converter.storage.remove_currency")
    def test_remove_currency(self, mock_remove_currency):
        mock_remove_currency.return_value = [self.USD]
        resp = remove_currency(self.EUR)
        self.assertEqual(mock_remove_currency.call_count, 1)
        self.assertEqual(mock_remove_currency.call_args[0][0], self.EUR)
        self.assertEqual(resp, mock_remove_currency.return_value)

    @mock.patch.object(OpenExchangeRates, "get_rates")
    @mock.patch("converter.storage.set_rates")
    @mock.patch("converter.storage.get_currencies")
    def test_update_rates(self, mock_get_currencies, mock_set_rates, mock_get_rates):
        eur_usd = {self.EUR: Decimal(1),
                   self.USD: self.EUR_USD}
        usd_eur = {self.USD: Decimal(1),
                   self.EUR: self.USD_EUR}

        def mock_rate_response(currency):
            if currency == self.EUR:
                return eur_usd
            elif currency == self.USD:
                return usd_eur

        mock_get_rates.side_effect = mock_rate_response
        mock_get_currencies.return_value = [self.EUR, self.USD]
        resp = update_rates()

        self.assertEqual(mock_get_currencies.call_count, 1)

        self.assertEqual(mock_get_rates.call_count, 2)
        self.assertEqual(mock_get_rates.call_args_list[0][0][0], self.EUR)
        self.assertEqual(mock_get_rates.call_args_list[1][0][0], self.USD)

        self.assertEqual(mock_set_rates.call_count, 2)
        self.assertEqual(mock_set_rates.call_args_list[0][0][0], self.EUR)
        self.assertEqual(mock_set_rates.call_args_list[0][0][1], eur_usd)
        self.assertEqual(mock_set_rates.call_args_list[1][0][0], self.USD)
        self.assertEqual(mock_set_rates.call_args_list[1][0][1], usd_eur)
