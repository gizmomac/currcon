from decimal import Decimal
from tests.base import TestBase
from converter.storage import get_rate, set_rates, get_currencies, add_currency, remove_currency
from app import redis_client
from converter.storage import CURRENCIES


class CliTest(TestBase):

    def test_get_rate(self):
        rate = get_rate(self.EUR, self.USD)
        self.assertEqual(rate, self.EUR_USD)

    def test_set_rates(self):
        rate_huf_usd = '295.0'
        rate_huf_huf = '1'
        rates = {self.USD: Decimal(rate_huf_usd), self.HUF: Decimal(rate_huf_huf)}

        resp = set_rates(self.HUF, rates)
        self.assertEqual(resp, rates)
        self.assertEqual(redis_client.hget(self.HUF, self.USD), rate_huf_usd)
        self.assertEqual(redis_client.hget(self.HUF, self.HUF), rate_huf_huf)

    def test_set_rates_cleanup_unused_rates(self):
        rate_huf_usd = "295.0"
        rate_huf_huf = "1"
        rate_huf_gbp = "363.16"

        rates1 = {self.USD: Decimal(rate_huf_usd), self.HUF: Decimal(rate_huf_huf), self.GBP: Decimal(rate_huf_gbp)}
        resp = set_rates(self.HUF, rates1)
        self.assertEqual(resp, rates1)

        rates2 = {self.USD: Decimal(rate_huf_usd), self.HUF: Decimal(rate_huf_huf)}
        resp = set_rates(self.HUF, rates2)
        self.assertEqual(resp, rates2)

    def test_get_currencies(self):
        resp = get_currencies()
        self.assertEqual(set(resp), set([self.USD, self.HUF, self.EUR]))

    def test_add_currency(self):
        resp = add_currency(self.GBP)
        self.assertIn(self.GBP, resp)

    def test_remove_currency(self):
        redis_client.sadd(CURRENCIES, self.GBP)
        resp = remove_currency(self.GBP)
        self.assertNotIn(self.GBP, resp)
