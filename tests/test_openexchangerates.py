import mock
from decimal import Decimal
from tests.base import TestBase
from converter.openexchangerates import OpenExchangeRates
from tests.helper import LATEST_RESPONSE_OK, LATEST_RESPONSE_NO_RATES, LATEST_ERROR, LATEST_FREE
from converter.exceptions import CommunicationError
from requests.exceptions import RequestException


class OpenExchangeRateTest(TestBase):
    FAKE_API_KEY = "fakeapikey"

    def test_init(self):
        currencies = [self.EUR, self.USD]
        client = OpenExchangeRates(self.FAKE_API_KEY, currencies)
        self.assertEqual(client.app_id, self.FAKE_API_KEY)
        self.assertEqual(client.currencies, currencies)

    @mock.patch.object(OpenExchangeRates, "_latest")
    def test_get_rates_paid(self, mock_latest):
        eur_eur_rate = Decimal("1")
        eur_huf_rate = Decimal("325.0")
        mock_latest.return_value = {self.EUR.upper(): eur_eur_rate,
                                    self.USD.upper(): self.EUR_USD,
                                    self.HUF.upper(): eur_huf_rate}
        currencies = [self.EUR, self.USD]

        client = OpenExchangeRates(self.FAKE_API_KEY, currencies, paid=True)
        response = client.get_rates(self.EUR)
        self.assertEqual(list(response.keys()), currencies)
        self.assertEqual(response[self.EUR], eur_eur_rate)
        self.assertEqual(response[self.USD], self.EUR_USD)

    @mock.patch.object(OpenExchangeRates, "_latest")
    def test_get_rates(self, mock_latest):
        mock_latest.return_value = LATEST_FREE['rates']
        currencies = [self.EUR, self.HUF]

        client = OpenExchangeRates(self.FAKE_API_KEY, currencies, paid=False)
        response = client.get_rates(self.HUF)

        usd_huf = LATEST_FREE['rates'][self.HUF.upper()]
        usd_eur = LATEST_FREE['rates'][self.EUR.upper()]

        self.assertEqual(list(response.keys()), currencies)
        self.assertEqual(response[self.EUR], Decimal(usd_huf) / Decimal(usd_eur))
        self.assertEqual(response[self.HUF], Decimal("1.0"))

    @mock.patch('requests.get')
    def test_latest_ok(self, mock_get):
        mock_response = mock.Mock()
        mock_response.status_code = 200
        mock_response.json.return_value = LATEST_RESPONSE_OK
        mock_get.return_value = mock_response

        currencies = [self.EUR, self.USD]
        client = OpenExchangeRates(self.FAKE_API_KEY, currencies)
        resp = client._latest(self.EUR)
        self.assertEqual(resp, LATEST_RESPONSE_OK["rates"])

    @mock.patch('requests.get')
    def test_latest_missing_rates(self, mock_get):
        mock_response = mock.Mock()
        mock_response.status_code = 200
        mock_response.json.return_value = LATEST_RESPONSE_NO_RATES
        mock_get.return_value = mock_response

        currencies = [self.EUR, self.USD]
        client = OpenExchangeRates(self.FAKE_API_KEY, currencies)
        with self.assertRaises(CommunicationError):
            client.get_rates(self.EUR)

    @mock.patch('requests.get')
    def test_latest_error(self, mock_get):
        mock_response = mock.Mock()
        mock_response.raise_for_status.side_effect = RequestException
        mock_response.status_code = 401
        mock_response.json.return_value = LATEST_ERROR
        mock_get.return_value = mock_response

        currencies = [self.EUR, self.USD]
        client = OpenExchangeRates(self.FAKE_API_KEY, currencies)
        with self.assertRaises(CommunicationError):
            client.get_rates(self.EUR)
