from app import app, redis_client
from converter.storage import CURRENCIES
from decimal import Decimal
import unittest


class TestBase(unittest.TestCase):
    USD = 'usd'
    HUF = 'huf'
    EUR = 'eur'
    GBP = 'gbp'
    USD_EUR = Decimal('0.9')
    EUR_USD = Decimal('1.1')

    @classmethod
    def setUpClass(cls):
        app.config.from_object('settings.testing')
        cls.app = app.test_client()
        redis_client.init_app(app)

    def setUp(self):
        self._add_currency(self.USD)
        self._add_currency(self.HUF)
        self._add_currency(self.EUR)
        self._add_rate(self.USD, self.EUR, self.USD_EUR)
        self._add_rate(self.EUR, self.USD, self.EUR_USD)

    def tearDown(self):
        redis_client.flushdb()

    @staticmethod
    def _add_currency(currency):
        redis_client.sadd(CURRENCIES, currency.lower())

    @staticmethod
    def _add_rate(curr_from, curr_to, rate):
        redis_client.hset(curr_from.lower(), curr_to.lower(), str(rate))

    def test_invalid_endpoint(self):
        rv = self.app.get(f"/invalid/endpoint")
        self.assertEqual(rv.status_code, 404)
