from decimal import Decimal
from freezegun import freeze_time
from tests.base import TestBase


class ApiTest(TestBase):

    def test_invalid_endpoint(self):
        rv = self.app.get(f"/invalid/endpoint")
        self.assertEqual(rv.status_code, 404)

    @freeze_time("2012-01-14T12:00:01.000000")
    def test_invalid_currency(self):
        currency = self.GBP
        amount = '10.00'
        rv = self.app.get(f"/api/convert/{amount}/{currency}/{currency}")
        self.assertEqual(rv.json['meta']['timestamp'], "2012-01-14T12:00:01.000000")
        self.assertEqual(rv.status_code, 400)
        self.assertEqual(rv.json["error"], f"Invalid currency_code: {currency}")

    @freeze_time("2012-01-14T12:00:01.000000")
    def test_invalid_amount(self):
        currency = self.GBP
        amount = 'sfmdomio'
        rv = self.app.get(f"/api/convert/{amount}/{currency}/{currency}")
        self.assertEqual(rv.json['meta']['timestamp'], "2012-01-14T12:00:01.000000")
        self.assertEqual(rv.status_code, 400)
        self.assertEqual(rv.json["error"], f"Invalid amount: {amount}")

    @freeze_time("2012-01-14T12:00:01.000000")
    def test_valid_currency_without_rate(self):
        currency = self.HUF
        amount = '10.00'
        rv = self.app.get(f"/api/convert/{amount}/{currency}/{currency}")
        self.assertEqual(rv.status_code, 400)
        self.assertEqual(rv.json["error"], "Missing conversion rate")
        self.assertEqual(rv.json['meta']['timestamp'], "2012-01-14T12:00:01.000000")

    @freeze_time("2012-01-14T12:00:01.000000")
    def test_valid_currency_with_rate(self):
        currency_from = self.EUR
        currency_to = self.USD
        amount = '10.00'
        rv = self.app.get(f"/api/convert/{amount}/{currency_from}/{currency_to}")
        self.assertEqual(rv.status_code, 200)
        self.assertEqual(str(rv.json['meta']['rate']), str(self.EUR_USD))
        self.assertEqual(rv.json['meta']['timestamp'], "2012-01-14T12:00:01.000000")
        self.assertEqual(rv.json['response'], Decimal(amount) * self.EUR_USD)
